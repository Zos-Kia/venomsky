const fs = require('fs')
const { parse, writeUncompressed } = require('prismarine-nbt')
const { cpuUsage } = require('process')

const cellSize = 128

/**
 * Config
 */
const centerX = -129867
const centerY = -359471

// const template = 'big'
// const widthCells = 7
// const heightCells = 10
const template = 'small'
const widthCells = 2
const heightCells = 3

/**
 * Constants
 */
const width = widthCells * cellSize
const height = heightCells * cellSize

const startX = centerX - width / 2
const startY = centerY - height / 2

// const actualPalette = [{
//   Name: {
//     type: 'string',
//     value: 'minecraft:air'
//   }
// }, {
//   Name: {
//     type: 'string',
//     value: 'minecraft:obsidian'
//   }
// }]

// 22h50-23h05 : 0-0
// 15 min => 1300 blocks
// 23h07-23h37 : 0-1
// 30 min => 4600 blockse

    // Open the nbt
const cellData = fs.readFileSync(`./venom_small.nbt`)

parse(cellData).then(({parsed, type}) => {  
  // parsed.value.palette.value.value = actualPalette
  // Filter out filler blocks (cobweb)
  const obsidianBlocks = parsed.value.blocks.value.value
    .filter(block => block.state.value !== 0)

  // Ignore schematic if no obsidian block
  if (obsidianBlocks.length > 0) {
    // Actually remove filler blocks from nbt and save
    parsed.value.blocks.value.value = obsidianBlocks
    const cellName = `./venom_fixed_small.nbt`
    
    const outBuffer = fs.createWriteStream(cellName)
    const newBuf = writeUncompressed(parsed, type)
    outBuffer.write(newBuf)
    outBuffer.end(() => console.log(`Done writing ${cellName}`))
  } else {
    console.log(`${x} ${y} Nothing to do`)
  }
})


// nbt.parse(data, (err, data) => {
//   if (err) { throw err }

//   data.value.palette.value.value = data.value.palette.value.value.map(
//     block => ({
//       Name: {
//         ...block.Name,
//         value: block.Name.value === 'minecraft:cobweb' ? 'minecraft:air' : block.Name.value
//       }
//     })
//   )
//   console.log(data.value.palette.value.value)
//   const writer = new nbt.Writer()
//   writer.compound(data)
//   fs.writeFileSync('./test.nbt', writer.buffer)
// })