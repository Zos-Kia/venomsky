
# Required mods

- VenomHack (up-to date)
- [Malilib for 1.16](https://www.curseforge.com/minecraft/mc-mods/malilib/files/all?filter-game-version=1738749986%3a70886)
- [Litematica with Printer support](https://github.com/aleksilassila/litematica-printer/releases)

# Schematics

Copy them in the `schematics` folder of your minecraft instance. (folder is created the first time litematica is started)

| _ | _ |
| ------ | ------ |
| [Quadrant 0/0](https://gitlab.com/Zos-Kia/venomsky/-/raw/master/huge/schematics/venom_flat_0_0_-1009_-1778.schematic) | [Quadrant 1/0](https://gitlab.com/Zos-Kia/venomsky/-/raw/master/huge/schematics/venom_flat_1_0_15_-1778.schematic) |
| [Quadrant 0/1](https://gitlab.com/Zos-Kia/venomsky/-/raw/master/huge/schematics/venom_flat_0_1_-1009_-370.schematic) | [Quadrant 1/1](https://gitlab.com/Zos-Kia/venomsky/-/raw/master/huge/schematics/venom_flat_1_1_15_-370.schematic) |

# How this works

We use baritone to walk us through the build, and Litematica printer to fast-place obsidian blocks as far as we can reach. Each one can be used on its own : baritone is slow but entirely AFK, printer is fast but you need to walk it through the build, both together is fast but it eats up inventory like crazy.

## Litematica

* In Minecraft, press `M + C` (or `, + C` on AZERTY keyboard) to open Litematica config
* Set `printingMode` to true
    * you can play around with `printingDelay` (smaller is faster) and `printingRange`
* Exit config, and press `M` (or `,`)
* Click on `Load schematics`
* Select the schematic of a quadrant and click on `Load schematic`
* Go to `Schematic placements`
* Click on `Configure`
* In placement origin on the right copy the exact coordinates that are in the file name of the schematic
    * Example : venom_flat_0_0_-1009_-1778.schematic
    * X: -1009 / Y: 255 / Z: -1778

By flying around you should now see the hologram of the build, and check that it's at the right position.

## Baritone

* Before you start you should always have some obsidian and cobblestone stacks in your hotbar
* Turn on `Auto Replenish` in Venomhack
* Using the hologram, pick a starting point high in the air and not too far from where obsidian blocks are actually going to be. On top of a lavacast or even on the teacup should do just fine. Remember to bring enough cobblestone to bridge to the first obsidian blocks.
* Copy this command in chat :
    * Quadrant 0/0 : `.baritone build venom_flat_0_0_-1009_-1778 -1009 255 -1778`
    * Quadrant 1/0 : `.baritone build venom_flat_1_0_15_-1778 15 255 -1778`
    * Quadrant 0/1 : `.baritone build venom_flat_0_1_-1009_-370 -1009 255 -370`
    * Quadrant 1/1 : `.baritone build venom_flat_1_1_15_-370 15 255 -370`
* `.baritone pause` / `.baritone resume` to pause / resume
* `.baritone stop` to stop

Sometimes Baritone will get all monkey and start walking hundreds of blocks to the other side of the schematic for no apparent reason. In that case just pause it, go to a place in the schematic where a lot of obsidian is missing, and resume.
