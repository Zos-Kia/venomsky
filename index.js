const fs = require('fs')
const { parse, writeUncompressed } = require('prismarine-nbt')

const cellSizeX = 1024
const cellSizeY = 1408

/**
 * Config
 */
const centerX = 0
const centerY = 0

// const template = 'big'
// const widthCells = 7
// const heightCells = 10
const template = 'huge'
const widthCells = 2
const heightCells = 2

/**
 * Constants
 */
const width = widthCells * cellSizeX
const height = heightCells * cellSizeY

// const startX = centerX - width / 2
// const startY = centerY - height / 2
const startX = -1009
const startY = -1778

// const actualPalette = [{
//   Name: {
//     type: 'string',
//     value: 'minecraft:air'
//   }
// }, {
//   Name: {
//     type: 'string',
//     value: 'minecraft:obsidian'
//   }
// }]

// 22h50-23h05 : 0-0
// 15 min => 1300 blocks
// 23h07-23h37 : 0-1
// 30 min => 4600 blockse

for (let y = 0;y < heightCells;y++) {
  for (let x = 0;x < widthCells;x++) {
    // Open the nbt
    const cellData = fs.readFileSync(`./${template}/splits/venom_flat_split_${x}_${y}.nbt`)
    const cellStartX = startX + (cellSizeX * x)
    const cellStartY = startY + (cellSizeY * y)

    parse(cellData).then(({parsed, type}) => {  
      // parsed.value.palette.value.value = actualPalette
      // Filter out filler blocks (cobweb)
      const obsidianBlocks = parsed.value.blocks.value.value
        .filter(block => block.state.value !== 0)

      // Ignore schematic if no obsidian block
      if (obsidianBlocks.length > 0) {
        // Actually remove filler blocks from nbt and save
        parsed.value.blocks.value.value = obsidianBlocks
        const cellName = `./${template}/fixed/venom_flat_${x}_${y}_${cellStartX}_${cellStartY}.nbt`
        
        const outBuffer = fs.createWriteStream(cellName)
        const newBuf = writeUncompressed(parsed, type)
        outBuffer.write(newBuf)
        outBuffer.end(() => console.log(`Done writing ${cellName}`))
      } else {
        console.log(`${x} ${y} Nothing to do`)
      }
    })
  }
}


// nbt.parse(data, (err, data) => {
//   if (err) { throw err }

//   data.value.palette.value.value = data.value.palette.value.value.map(
//     block => ({
//       Name: {
//         ...block.Name,
//         value: block.Name.value === 'minecraft:cobweb' ? 'minecraft:air' : block.Name.value
//       }
//     })
//   )
//   console.log(data.value.palette.value.value)
//   const writer = new nbt.Writer()
//   writer.compound(data)
//   fs.writeFileSync('./test.nbt', writer.buffer)
// })